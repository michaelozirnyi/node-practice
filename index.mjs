import * as packageUtils from 'test-utils';
import utils from 'util';

const params = { password: 'sdf234fdsf32cdsc' };

packageUtils.runMePlease(params, response => {
    console.log(response);
});

const runMePleasePromises = utils.promisify(packageUtils.runMePlease);

try {
    const result = await runMePleasePromises(params);
    console.log(result);
} catch (error) {
    console.log(error);
}
